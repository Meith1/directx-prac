#pragma once

#include <d3d11.h>
#include <DirectXMath.h>

#include "DirectXGame.h"
#include "Vertex.h"

class Mesh
{
public:

	Mesh();
	Mesh(Vertex* vertex, int numVertices, unsigned int* indexArray, int numIndices, ID3D11Device* device);
	Mesh(char* filename, ID3D11Device* device);
	~Mesh();

	ID3D11Buffer* GetVertexBuffer();
	ID3D11Buffer* GetIndexBuffer();
	int GetIndexCount();

private:

	ID3D11Buffer* _vertexBuffer;
	ID3D11Buffer* _indexBuffer;
	int _numIndices;
};

