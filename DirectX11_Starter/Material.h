#pragma once
#include "SimpleShader.h"

#include <D3D11.h>
class Material
{
public:
	Material();
	Material(SimplePixelShader* pixelShader, SimpleVertexShader* vertexShader);
	~Material();

	SimplePixelShader* getPixelShader() { return _pixelShader; }
	SimpleVertexShader* getVertexShader() { return _vertexShader; }

private:
	SimplePixelShader* _pixelShader;
	SimpleVertexShader* _vertexShader;
};

