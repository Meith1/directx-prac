#ifndef __GameEntity__
#define __GameEntity__

#include <DirectXMath.h>

#include "Mesh.h"

using namespace DirectX;

class GameEntity
{
public:
	GameEntity();
	GameEntity(Mesh* mesh);
	~GameEntity();

	void setWorldMatrix(XMFLOAT4X4 worldMatrix);
	XMFLOAT4X4 getWorldMatrix();

	void setPosition(XMFLOAT3 position);
	XMFLOAT3 getPosition();

	void setRotation(XMFLOAT3 rotation);
	XMFLOAT3 getRotation();

	void setScale(XMFLOAT3 scale);
	XMFLOAT3 getScale();

	Mesh* getMesh()
	{
		return _mesh;
	}

	void update();

private:
	XMFLOAT4X4 _worldMatrix;
	XMFLOAT4X4 _translationMatrix;
	XMFLOAT4X4 _scaleMatrix;
	XMFLOAT4X4 _rotationMatrix;


	XMFLOAT3 _position;
	XMFLOAT3 _rotation;
	XMFLOAT3 _scale;

	Mesh* _mesh;
};

#endif