// ----------------------------------------------------------------------------
//  A few notes on project settings
//
//  - The project is set to use the UNICODE character set
//    - This was changed in Project Properties > Config Properties > General > Character Set
//    - This basically adds a "#define UNICODE" to the project
//
//  - The include directories were automagically correct, since the DirectX 
//    headers and libs are part of the windows SDK
//    - For instance, $(WindowsSDK_IncludePath) is set as a project include 
//      path by default.  That's where the DirectX headers are located.
//
//  - Two libraries had to be manually added to the Linker Input Dependencies
//    - d3d11.lib
//    - d3dcompiler.lib
//    - This was changed in Project Properties > Config Properties > Linker > Input > Additional Dependencies
//
//  - The Working Directory was changed to match the actual .exe's 
//    output directory, since we need to load the compiled shader files at run time
//    - This was changed in Project Properties > Config Properties > Debugging > Working Directory
//
// ----------------------------------------------------------------------------

#include <Windows.h>
#include <d3dcompiler.h>
#include "MyDemoGame.h"

#pragma region Win32 Entry Point (WinMain)

// Win32 Entry Point
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{
	// Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

	// Make the game, initialize and run
	MyDemoGame game(hInstance);
	
	// If we can't initialize, we can't run
	if( !game.Init() )
		return 0;
	
	// All set to run the game
	return game.Run();
}

#pragma endregion

#pragma region Constructor / Destructor

MyDemoGame::MyDemoGame(HINSTANCE hInstance) : DirectXGame(hInstance)
{
	// Set up our custom caption and window size
	windowCaption = L"Demo DX11 Game";
	windowWidth = 800;
	windowHeight = 600;
}

MyDemoGame::~MyDemoGame()
{
	// Release all of the D3D stuff that's still hanging out
//	ReleaseMacro(vertexBuffer);
	//ReleaseMacro(indexBuffer);
	/*ReleaseMacro(vertexShader);
	ReleaseMacro(pixelShader);
	ReleaseMacro(vsConstantBuffer);
	ReleaseMacro(inputLayout);*/

	delete _mesh1;
	delete _mesh2;
	delete _mesh3;
	delete _mesh4;

	delete _camera;

	for (std::vector<GameEntity*>::iterator i = _transformation.begin(); i != _transformation.end(); i++)
		delete *i;

	delete pixelShader;
	delete vertexShader;
	delete _material;
}

#pragma endregion

#pragma region Initialization

// Initializes the base class (including the window and D3D),
// sets up our geometry and loads the shaders (among other things)
bool MyDemoGame::Init()
{
	// Make sure DirectX initializes properly
	if( !DirectXGame::Init() )
		return false;

	// Create the necessary DirectX buffers to draw something
	CreateGeometryBuffers();

	// Load pixel & vertex shaders, and then create an input layout
	LoadShadersAndInputLayout();

	// Set up camera-related matrices
	InitializeCameraMatrices();

	// Set up world matrix
	// In an actual game, each object will need one of these and they should
	//  update when/if the object moves (every frame)
	XMMATRIX W = XMMatrixIdentity();
	XMStoreFloat4x4(&worldMatrix, XMMatrixTranspose(W));

	light.AmbientColor = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
	light.DiffuseColor = XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f);
	light.Direction = XMFLOAT3(1.0f, -1.0f, 0.0f);

	newLight.AmbientColor = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
	newLight.DiffuseColor = XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f);
	newLight.Direction = XMFLOAT3(-1.0f, 1.0f, 0.0f);

	// Successfully initialized
	return true;
}

// Creates the vertex and index buffers for a single triangle
void MyDemoGame::CreateGeometryBuffers()
{
	// Create some temporary variables to represent colors
	XMFLOAT4 red	= XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
	XMFLOAT4 green	= XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);
	XMFLOAT4 blue	= XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f);

	// Create some temp variables to represent normals
	XMFLOAT3 norm = XMFLOAT3(0.0f, 0.0f, -1.0f);

	// Create temp variable for txture
	XMFLOAT2 uv = XMFLOAT2(0.0f, 0.0f);

	// Set up the vertices we want to put into the Vertex Buffer

	Vertex vertices1[] = 
	{
		{ XMFLOAT3(-2.0f, -1.0f, +0.0f), norm, uv },
		{ XMFLOAT3(-1.0f, +1.0f, +0.0f), norm, uv },
		{ XMFLOAT3(-1.0f, -1.0f, +0.0f), norm, uv }
	};

	Vertex vertices2[] =
	{
		{ XMFLOAT3(-1.0f, +1.0f, +0.0f), norm, uv },
		{ XMFLOAT3(+1.0f, +1.0f, +0.0f), norm, uv },
		{ XMFLOAT3(+0.0f, -1.0f, +0.0f), norm, uv }
	};

	Vertex vertices3[] =
	{
		{ XMFLOAT3(+1.0f, +1.0f, +0.0f), norm, uv },
		{ XMFLOAT3(+2.0f, -1.0f, +0.0f), norm, uv },
		{ XMFLOAT3(+1.0f, -1.0f, +0.0f), norm, uv }
	};
	
	// Set up the indices of the vertices (necessary for indexed drawing)
	UINT indices1[] = { 0, 1, 2 };

	_mesh1 = new Mesh(vertices1, 3, indices1, 3, device);
	_mesh2 = new Mesh(vertices2, 3, indices1, 3, device);
	_mesh3 = new Mesh(vertices3, 3, indices1, 3, device);

	_mesh4 = new Mesh("helix.obj", device);

	_transformation.push_back(new GameEntity(_mesh1));
	_transformation.push_back(new GameEntity(_mesh2));
	_transformation.push_back(new GameEntity(_mesh3));
	_transformation.push_back(new GameEntity(_mesh4));

	XMFLOAT3 rotation = XMFLOAT3(0.f, 1.f, 0.0f);
	
	for (int i = 0; i < 3; i++)
	{
		_transformation[i]->setRotation(rotation);
	}

	_transformation[3]->setPosition(XMFLOAT3(5,0,5));
}

// Loads shaders from compiled shader object (.cso) files, and uses the
// vertex shader to create an input layout which is needed when sending
// vertex data to the device
void MyDemoGame::LoadShadersAndInputLayout()
{
	//// Set up the vertex layout description
	//// This has to match the vertex input layout in the vertex shader
	//// We can't set up the input layout yet since we need the actual vert shader
	//D3D11_INPUT_ELEMENT_DESC vertexDesc[] =
	//{
	//	{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,	0, 0,	D3D11_INPUT_PER_VERTEX_DATA, 0},
	//	{"NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12,	D3D11_INPUT_PER_VERTEX_DATA, 0},
	//	{"TEXCOORD", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0}
	//};

	//// Load Vertex Shader --------------------------------------
	//ID3DBlob* vsBlob;
	//D3DReadFileToBlob(L"VertexShader.cso", &vsBlob);

	//// Create the shader on the device
	//HR(device->CreateVertexShader(
	//	vsBlob->GetBufferPointer(),
	//	vsBlob->GetBufferSize(),
	//	NULL,
	//	&vertexShader));

	//// Before cleaning up the data, create the input layout
	//HR(device->CreateInputLayout(
	//	vertexDesc,
	//	ARRAYSIZE(vertexDesc),
	//	vsBlob->GetBufferPointer(),
	//	vsBlob->GetBufferSize(),
	//	&inputLayout));

	//// Clean up
	//ReleaseMacro(vsBlob);

	//// Load Pixel Shader ---------------------------------------
	//ID3DBlob* psBlob;
	//D3DReadFileToBlob(L"PixelShader.cso", &psBlob);

	//// Create the shader on the device
	//HR(device->CreatePixelShader(
	//	psBlob->GetBufferPointer(),
	//	psBlob->GetBufferSize(),
	//	NULL,
	//	&pixelShader));

	//// Clean up
	//ReleaseMacro(psBlob);

	//// Constant buffers ----------------------------------------
	//D3D11_BUFFER_DESC cBufferDesc;
	//cBufferDesc.ByteWidth           = sizeof(dataToSendToVSConstantBuffer);
	//cBufferDesc.Usage				= D3D11_USAGE_DEFAULT;
	//cBufferDesc.BindFlags			= D3D11_BIND_CONSTANT_BUFFER;
	//cBufferDesc.CPUAccessFlags		= 0;
	//cBufferDesc.MiscFlags			= 0;
	//cBufferDesc.StructureByteStride = 0;
	//HR(device->CreateBuffer(
	//	&cBufferDesc,
	//	NULL,
	//	&vsConstantBuffer));

	pixelShader = new SimplePixelShader(device, deviceContext);
	vertexShader = new SimpleVertexShader(device, deviceContext);

	pixelShader->LoadShaderFile(L"PixelShader.cso");
	vertexShader->LoadShaderFile(L"VertexShader.cso");

	_material = new Material(pixelShader, vertexShader);
}

// Initializes the matrices necessary to represent our 3D camera
void MyDemoGame::InitializeCameraMatrices()
{
	// Create the View matrix
	// In an actual game, update this when the camera moves (every frame)
	XMVECTOR position = XMVectorSet(0, 0, -5, 0);
	XMVECTOR target = XMVectorSet(0, 0, 0, 0);
	XMVECTOR up = XMVectorSet(0, 1, 0, 0);
	XMMATRIX V = XMMatrixLookAtLH(position, target, up); // View matrix creation:
                                                         // Looks at a "target" from
	                                                     // a particular "position"
	XMStoreFloat4x4(&viewMatrix, XMMatrixTranspose(V));

	// Create the Projection matrix
	// This should match the window's aspect ratio, and also update anytime
	// the window resizes (which is already happening in OnResize() below)
	XMMATRIX P = XMMatrixPerspectiveFovLH(
		0.25f * 3.1415926535f,		// Field of View Angle
		AspectRatio(),				// Aspect ratio
		0.1f,						// Near clip plane distance
		100.0f);					// Far clip plane distance
	XMStoreFloat4x4(&projectionMatrix, XMMatrixTranspose(P));


	// new camera stuff

	_camera = new Camera(AspectRatio());
	_camera->setCamPosition(XMVectorSet(0, 0, -5, 0));
	_camera->setCamDirection(XMVectorSet(0, 0, 1, 0));
}

#pragma endregion

#pragma region Window Resizing

// Handles resizing the window and updating our projection matrix to match
void MyDemoGame::OnResize()
{
	// Handle base-level DX resize stuff
	DirectXGame::OnResize();

	// Update our projection matrix since the window size changed
	XMMATRIX P = XMMatrixPerspectiveFovLH(
		0.25f * 3.1415926535f,
		AspectRatio(),
		0.1f,
		100.0f);
	XMStoreFloat4x4(&projectionMatrix, XMMatrixTranspose(P));
}
#pragma endregion

#pragma region Game Loop

// Update your game state
void MyDemoGame::UpdateScene(float dt)
{
	// Take input, update game logic, etc.
	for (int i = 0; i < 4; i++)
	{
		_transformation[i]->update();
	}

	_camera->update();
}

// Clear the screen, redraw everything, present
void MyDemoGame::DrawScene()
{
	// Background color (Cornflower Blue in this case) for clearing
	const float color[4] = {0.4f, 0.6f, 0.75f, 0.0f};

	// Clear the buffer (erases what's on the screen)
	//  - Do this once per frame
	//  - At the beginning (before drawing anything)
	deviceContext->ClearRenderTargetView(renderTargetView, color);
	deviceContext->ClearDepthStencilView(
		depthStencilView, 
		D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
		1.0f,
		0);

	// Set up the input assembler
	//  - These technically don't need to be set every frame, unless you're changing the
	//    input layout (different kinds of vertices) or the topology (different primitives)
	//    between draws
	deviceContext->IASetInputLayout(inputLayout);
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	
	// Set the current vertex and pixel shaders
	//  - These don't need to be set every frame YET
	//  - Once you have multiple shaders, you will need to change these
	//    between drawing objects that will use different shaders
	/*deviceContext->VSSetShader(vertexShader, NULL, 0);
	deviceContext->PSSetShader(pixelShader, NULL, 0);*/

	////old camera stuff
	//dataToSendToVSConstantBuffer.view = viewMatrix;
	//dataToSendToVSConstantBuffer.projection = projectionMatrix;

	// new camera stuff
	//dataToSendToVSConstantBuffer.view = _camera->getViewMatrix();
	//dataToSendToVSConstantBuffer.projection = _camera->getProjectionMatrix();

	_material->getVertexShader()->SetMatrix4x4("view", _camera->getViewMatrix());
	_material->getVertexShader()->SetMatrix4x4("projection", _camera->getProjectionMatrix());

	for (int i = 0; i < 4; i++)
	{

		// Copy CPU-side data to a single CPU-side structure
		//  - Allows us to send the data to the GPU buffer in one step
		//  - Do this PER OBJECT, before drawing 
		/*(dataToSendToVSConstantBuffer.world) = _transformation[i]->getWorldMatrix();*/
		_material->getVertexShader()->SetMatrix4x4("world", _transformation[i]->getWorldMatrix());
		
		// Update the GPU-side constant buffer with our single CPU-side structure
		//  - Faster than setting individual sub-variables multiple times
		//  - Do this PER OBJECT, before drawing it
		/*deviceContext->UpdateSubresource(
			vsConstantBuffer,
			0,
			NULL,
			&dataToSendToVSConstantBuffer,
			0,
			0);*/

		// Set the constant buffer to be used by the Vertex Shader
		//  - This should be done PER OBJECT you intend to draw, as each object
		//    will probably have different data to send to the shader (matrices
		//    in this case)
		//deviceContext->VSSetConstantBuffers(
		//	0,	// Corresponds to the constant buffer's register in the vertex shader
		//	1,
		//	&vsConstantBuffer);

		_material->getPixelShader()->SetData(
			"light",  // The name of the (eventual) variable in the shader
			&light,   // The address of the data to copy
			sizeof(DirectionalLight)); // The size of the data to copy

		_material->getPixelShader()->SetData(
			"newlight",  // The name of the (eventual) variable in the shader
			&newLight,   // The address of the data to copy
			sizeof(DirectionalLight)); // The size of the data to copy

		_material->getVertexShader()->SetShader();
		_material->getPixelShader()->SetShader();

		// Set buffers in the input assembler
		//  - This should be done PER OBJECT you intend to draw, as each object could
		//    potentially have different geometry (and therefore different buffers!)
		//  - You must have both a vertex and index buffer set to draw
		UINT stride = sizeof(Vertex);
		UINT offset = 0;

		ID3D11Buffer* tempbuf;
		tempbuf = _transformation[i]->getMesh()->GetVertexBuffer();
		deviceContext->IASetVertexBuffers(0, 1, &tempbuf, &stride, &offset);

		deviceContext->IASetIndexBuffer(_transformation[i]->getMesh()->GetIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);

		deviceContext->DrawIndexed(
			_transformation[i]->getMesh()->GetIndexCount(),	// The number of indices we're using in this draw
			0,
			0);
	}
	// Present the buffer
	//  - Puts the stuff on the screen
	//  - Do this EXACTLY once per frame
	//  - Always at the end of the frame
	HR(swapChain->Present(0, 0));
}

#pragma endregion

#pragma region Mouse Input

// These methods don't do much currently, but can be used for mouse-related input

void MyDemoGame::OnMouseDown(WPARAM btnState, int x, int y)
{
	prevMousePos.x = x;
	prevMousePos.y = y;

	SetCapture(hMainWnd);
}

void MyDemoGame::OnMouseUp(WPARAM btnState, int x, int y)
{
	ReleaseCapture();
}

void MyDemoGame::OnMouseMove(WPARAM btnState, int x, int y)
{
	
	_camera->setx(((y - prevMousePos.y) / 1000.f));
	_camera->sety(((x - prevMousePos.x) / 1000.f));
	
	prevMousePos.x = x;
	prevMousePos.y = y;
}
#pragma endregion