#pragma once

#include <DirectXMath.h>

using namespace DirectX;

class Camera
{
public:
	Camera(float aspectRatio);
	~Camera();

	void setViewMatrix(XMFLOAT4X4 viewMatrix) { _viewMatrix = viewMatrix; }
	void setProjectionMatrix(XMFLOAT4X4 projectionMatrix) { _projectionMatrix = projectionMatrix; }

	XMFLOAT4X4 getViewMatrix() { return _viewMatrix; }
	XMFLOAT4X4 getProjectionMatrix() { return _projectionMatrix; }

	void setCamPosition(XMVECTOR camPosition) { _camPosition = camPosition; }
	void setCamDirection(XMVECTOR camDirection) { _camDirection = camDirection; }

	XMVECTOR getCamPosition() { return _camPosition; }
	XMVECTOR getCamDirection() { return _camDirection; }
	XMVECTOR getUpVector() { return _upVector; }

	float getx() { return _xRotation; }
	float gety() { return _yRotation; }

	void setx(float x) { _xRotation = x; }
	void sety(float y) { _yRotation = y; }

	void update();

private:

	XMFLOAT4X4 _viewMatrix;
	XMFLOAT4X4 _projectionMatrix;
	XMFLOAT4 forward;

	XMVECTOR _camPosition;
	XMVECTOR _camDirection;
	float _xRotation;
	float _yRotation;
	float _velocity;
	XMVECTOR _forwardVector;
	XMVECTOR _upVector;
};

