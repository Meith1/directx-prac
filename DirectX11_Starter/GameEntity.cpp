#include "GameEntity.h"

GameEntity::GameEntity()
{
}

GameEntity::GameEntity(Mesh* mesh)
{
	_mesh = mesh;

	_position = XMFLOAT3(0.0f, 0.0f, 0.0f);
	_rotation = XMFLOAT3(0.0f, 0.0f, 0.0f);
	_scale = XMFLOAT3(1.0f, 1.0f, 1.0f);
	
	update();
}

GameEntity::~GameEntity()
{
}

void GameEntity::setWorldMatrix(XMFLOAT4X4 worldMatrix)
{

}

XMFLOAT4X4 GameEntity::getWorldMatrix()
{
	return _worldMatrix;
}

void GameEntity::setPosition(XMFLOAT3 position)
{
	_position = position;
}

XMFLOAT3 GameEntity::getPosition()
{
	return _position;
}

void GameEntity::setRotation(XMFLOAT3 rotation)
{
	_rotation = rotation;
}

XMFLOAT3 GameEntity::getRotation()
{
	return _rotation;
}

void GameEntity::setScale(XMFLOAT3 scale)
{
	_scale = scale;
}

XMFLOAT3 GameEntity::getScale()
{
	return _scale;
}

void GameEntity::update()
{
	_rotation.y += 0.01 * 3.14/ 180;
	XMStoreFloat4x4(&_translationMatrix, XMMatrixTranslation(_position.x, _position.y, _position.z));
	XMStoreFloat4x4(&_rotationMatrix, XMMatrixRotationRollPitchYaw(_rotation.y, _rotation.z, _rotation.x));
	XMStoreFloat4x4(&_scaleMatrix, XMMatrixScaling(_scale.x, _scale.y, _scale.z));

	XMMATRIX translationMatrix = XMMatrixTranslation(_position.x, _position.y, _position.z);
	XMMATRIX rotationMatrix = XMMatrixRotationRollPitchYaw(_rotation.y, _rotation.z, _rotation.x);
	XMMATRIX scaleMatrix = XMMatrixScaling(_scale.x, _scale.y, _scale.z);

	XMStoreFloat4x4(&_worldMatrix, XMMatrixTranspose((XMMatrixMultiply(translationMatrix, XMMatrixMultiply(rotationMatrix, scaleMatrix)))));
}