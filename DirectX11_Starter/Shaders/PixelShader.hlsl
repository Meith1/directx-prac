
// Defines the input to this pixel shader
// - Should match the output of our corresponding vertex shader
struct VertexToPixel
{
	float4 position		: SV_POSITION;
	float3 normal		: NORMAL;
};

struct DirectionalLight
{
	float4 AmbientColor;
	float4 DiffuseColor;
	float3 Direction;
};

cbuffer perModel : register(b0)
{
	DirectionalLight light;
	DirectionalLight newlight;
}

// Entry point for this pixel shader
float4 main(VertexToPixel input) : SV_TARGET
{
	// Just return the input color
	// - Note that this color (like all values that pass through the rasterizer)
	//   is interpolated for each pixel between the corresponding 
	//   vertices of the triangle
	//return float4(1, 0, 0, 1);

	float3 negLight = light.Direction * -1;
	float amount = saturate(dot(input.normal, negLight));
	float4 surfaceColor = (light.DiffuseColor * amount) + light.AmbientColor;

	float3 newnegLight = newlight.Direction * -1;
	float newamount = saturate(dot(input.normal, newnegLight));
	float4 newsurfaceColor = (newlight.DiffuseColor * newamount) + newlight.AmbientColor;

	return float4(surfaceColor + newsurfaceColor);
}