#include "Camera.h"
#include <Windows.h>

Camera::Camera(float aspectRatio)
{
	_forwardVector = XMVectorSetByIndex(_forwardVector, 0.0f, 0);
	_forwardVector = XMVectorSetByIndex(_forwardVector, 0.0f, 1);
	_forwardVector = XMVectorSetByIndex(_forwardVector, 1.0f, 2);
	forward = { 0.0f, 0.0f, 1.0f, 0.0f };
	_upVector = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	XMMATRIX projectionMatrix = XMMatrixPerspectiveFovLH(0.25f * 3.1415926535f, aspectRatio, 0.1f, 100.0f);
	projectionMatrix = XMMatrixTranspose(projectionMatrix);
	XMStoreFloat4x4(&_projectionMatrix, projectionMatrix);

	_velocity = 0.01f;
	_xRotation = _yRotation = 0;
}

Camera::~Camera()
{
}

void Camera::update()
{
	XMVECTOR rotationQuaternion = XMQuaternionRotationRollPitchYaw(_xRotation, _yRotation, 0);
	
	XMVECTOR inverQuat = XMQuaternionInverse(rotationQuaternion);
	_forwardVector = XMLoadFloat4(&forward);
	_forwardVector = XMQuaternionMultiply(_forwardVector, rotationQuaternion);
	_forwardVector = XMQuaternionMultiply(inverQuat, _forwardVector );
	XMStoreFloat4(&forward, _forwardVector);

	_camDirection = _forwardVector;

	if (GetAsyncKeyState('W') & 0x8000)
		_camPosition += _camDirection*_velocity;

	if (GetAsyncKeyState('S') & 0x8000)
		_camPosition -= _camDirection*_velocity;

	if (GetAsyncKeyState('A') & 0x8000)
		_camPosition += XMVector3Cross(_camDirection, _upVector)*_velocity;

	if (GetAsyncKeyState('D') & 0x8000)
		_camPosition -= XMVector3Cross(_camDirection, _upVector)*_velocity;

	XMMATRIX lookToMatrix =  XMMatrixLookToLH(_camPosition, _camDirection, _upVector);
	lookToMatrix = XMMatrixTranspose(lookToMatrix);
	XMStoreFloat4x4(&_viewMatrix, lookToMatrix);
	_xRotation = _yRotation = 0;
}
